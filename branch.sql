CREATE TABLE branch(
  branch_id INT PRIMARY KEY,
  branch_location TEXT NOT NULL,
);

CREATE TABLE author(
  author_id INT PRIMARY KEY,
  author_name TEXT NOT NULL UNIQUE,
);

CREATE TABLE publisher( 
  publisher_id INT PRIMARY KEY,
  publisher_name TEXT NOT NULL UNIQUE
);

CREATE TABLE book(
  book_id INT PRIMARY KEY,
  book_title TEXT NOT NULL UNIQUE,
  isbn VARCHAR(13) NOT NULL,
  publisher_id INT,
  FOREIGN KEY (author_id) REFERENCES author(author_id),
  FOREIGN KEY (publisher_id) REFERENCES publisher(publisher_id),
);

CREATE TABLE book_authors(
  book_authors_id INT PRIMARY KEY,
  book_id INT,
  author_id INT,
  FOREIGN KEY (book_id) REFERENCES book(book_id),
  FOREIGN KEY (author_id) REFERENCES author(author_id),
)

CREATE TABLE `book_availabilty`(
  copy_id INT PRIMARY KEY,
  book_id INT,
  branch_id INT,
  number_of_copies INT NOT NULL,
  FOREIGN KEY (book_id) REFERENCES book(book_id),
  FOREIGN KEY (branch_id) REFERENCES branch(branch_id)
);