CREATE TABLE patient (
  patient_id INT PRIMARY KEY,
  patient_name TEXT NOT NULL,
  patient_DOB DATE,
  patient_age SMALLINT NOT NULL,
  patient_address TEXT,
);

CREATE TABLE doctor (
  doctor_id INT PRIMARY KEY, 
  doctor_name TEXT NOT NULL,
  Secretary_name TEXT NOT NULL
);

CREATE TABLE medicine (
  medicine_id INT PRIMARY KEY,
  medicine_name TEXT NOT NULL,
  medicine_brand TEXT
);

CREATE TABLE prescription (
  prescription_id INT PRIMARY KEY,
  appointment_date DATE,
  doctor_id INT,
  patient_id INT,
  FOREIGN KEY (doctor_id ) REFERENCES doctor(doctor_id ),
  FOREIGN KEY (patient_id) REFERENCES patient(patient_id)
);

CREATE TABLE dosage (
  dosage_id INT PRIMARY KEY,
  prescription_id INT,
  medicine_id INT,
  dosage_description TEXT NOT NULL,
  FOREIGN KEY (medicine_id) REFERENCES medicine(medicine_id),
  FOREIGN KEY (prescription_id) REFERENCES prescription(prescription_id)
);