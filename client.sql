CREATE TABLE location (
  location_id INT PRIMARY KEY,
  location_name VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE client (
  client_id INT PRIMARY KEY,
  client_name TEXT NOT NULL UNIQUE
);

CREATE TABLE client_location (
  client_location_id INT PRIMARY KEY,
  client_id INT,
  client_location INT,
  FOREIGN KEY(client_id) REFERENCES client(client_id),
  FOREIGN KEY(client_location) REFERENCES location(location_id)
);

CREATE TABLE contractor (
  contractor_id INT PRIMARY KEY,
  contractor_name TEXT NOT NULL UNIQUE
);

CREATE TABLE contractor_location (
  contractor_location_id INT PRIMARY KEY,
  contractor_id INT,
  contractor_location INT,
  FOREIGN KEY(contractor_id) REFERENCES contractor(contractor_id),
  FOREIGN KEY(contractor_location) REFERENCES location(location_id)
);

CREATE TABLE project_manager (
  manager_id INT PRIMARY KEY,
  manager_name TEXT NOT NULL UNIQUE,
  contractor_id INT,
  manager_location INT,
  FOREIGN KEY(contractor_id) REFERENCES contractor(contractor_id),
  FOREIGN KEY(manager_location) REFERENCES contractor_location(contractor_location_id)
);

CREATE TABLE staff (
  staff_id INT PRIMARY KEY,
  staff_name TEXT NOT NULL UNIQUE,
  manager_id INT,
  staff_location INT,
  FOREIGN KEY(manager_id) REFERENCES project_manager(manager_id),
  FOREIGN KEY(staff_location) REFERENCES project_manager(manager_location)
);

CREATE TABLE project_contract (
  project_id INT PRIMARY KEY,
  project_name VARCHAR(50),
  estimated_cost DECIMAL(10, 2) NOT NULL,
  completion_date DATE NOT NULL,
  project_location INT,
  client_id INT,
  contractor_id INT,
  FOREIGN KEY(project_location) REFERENCES location(location_id),
  FOREIGN KEY(client_id) REFERENCES client(client_id),
  FOREIGN KEY(contractor_id) REFERENCES contractor(contractor_id)
);